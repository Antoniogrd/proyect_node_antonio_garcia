const passport = require('passport');  // se requiere passport a sí mismo?
const LocalStrategy = require('passport-local').Strategy;
const User = require('../proyect-cars/models/User.model');
const bcrypt =require('bcrypt');

const saltRounds = 10;

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePass = (password) => {
    const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    return re.test(String(password));
}

// Como su nombre indica, es la estrategia que usaremos para el registro de nuestos usuarios
const registerStrategy = new LocalStrategy(
    {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
    },
    async (req, email, password, done) => {
        try {
            // Comprobamos si el usuario coincide con algún usuario registrado en la BD y lo almacenamos en una constante
            const existingUser = await User.findOne({ email: email });

            // Si existe el usuario, resopnderemos con un error notificándolo
            if (existingUser) {
                const error = new Error('The user already exists!');
                return done(error);
            }

            // Usamos la función creada arriba para validar que el formato de email es correcto
            const isValidEmail = validateEmail(email);

            // Si no es correcto, devolvemos erorr
            if (!isValidEmail) {
                const error = new Error('The email entered must have a valid email format')
                return done(error);
            }

            // Hacemos lo mismo con la contraseña, comprobamos si cumple los requisitos
            const isValidPassword = validatePass(password);

            // Y devolvemos error si no los cumple, con el correspondiente mensaje
            if (!isValidPassword) {
                const error = new Error('The password must contain 6 to 20 characters, an uppercase, a lowercase and a number');
                return done(error);
            }

            // Encriptamos la contraseña
            const hash = await bcrypt.hash(password, saltRounds);

            // Creamos el usuario y lo guardamos
            const newUser = new User({
                username: req.body.username,
                email: email,
                password: hash,
            });

            const savedUser = await newUser.save();

            return done(null, savedUser);

        } catch (error) {
            return done(error);
        }
    }
);

passport.use('registro', registerStrategy);

