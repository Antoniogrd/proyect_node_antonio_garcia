const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();



const DB_URL = process.env.DB_URL;

const DB_CONFIG = {
    useUnifiedTopology: true,
    useNewUrlParser: true,
}

const connect = () => {
    mongoose.connect(DB_URL, DB_CONFIG)

    .then((res) => {
        const { name, host } = res.connection;

        console.log(`Mongoose funcionando, ${name} en ${host}`);

    })
    .catch(error => {
        console.log('La conexión con Mongoose ha fallado, ejecuta "sudo systemctl start mongod" para solucionarlo', error);
    })
}

module.exports = { DB_URL, DB_CONFIG, connect };


