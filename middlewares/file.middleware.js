const multer = require('multer');
const path = require('path');

const ACCEPTED_FILENAME_EXTENSIONS = ['image/png', 'image/jpg', 'image/jpeg'];
const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-${file.originalname}`);
    },

    destination: (req, file, cb) => {
        const directory = path.join(__dirname, '../public/uploads');
        cb(null, directory);
    }

})

const fileFilter = (req, file, cb) => {
    if (ACCEPTED_FILENAME_EXTENSIONS.includes(file.mimetype)) {
        cb(null, true);

    } else {
        const error = new Error('Invalid file type');
        error.status(400);
        cb(error);
    }
};

const upload = multer({
    storage,
    fileFilter,
});

module.exports = { upload };