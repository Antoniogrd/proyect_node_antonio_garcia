const express = require('express');
const passport = require('passport');
const router = express.Router();
const indexController = require('../controllers/index.contoller')

router.get('/', indexController.indexGet);

module.exports = router;