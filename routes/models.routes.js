const express = require('express');
const router = express.Router();
const {isAdmin} = require('../middlewares/auth.middleware')
const controller = require('../controllers/models.controllers')

router.get('/', controller.modelsGet);

// Create
router.get('create', isAdmin, controller.createModelGet)
router.post('/create', isAdmin, controller.createModelPost);

// Read // hay que ponerle :id
router.get('/id', controller.idModelGet);

// Update
router.put('/edit', isAdmin, controller.updateModelPut);

// Delete
router.post('/:id', isAdmin, controller.deleteModelPost);

module.exports = router;