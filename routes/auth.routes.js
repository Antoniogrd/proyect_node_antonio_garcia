const express = require('express');
// const passport = require('passport');
const router = express.Router();
const controller = require('../controllers/auth.controller');

//REGISTRO
router.get('/register', controller.registerGet)
router.post('/register', controller.registerPost);


// LOGIN
router.get('/login', controller.loginGet)
router.post('/login', controller.loginPost);


// LOGOUT
router.post('/logout', controller.logoutPost);



module.exports = router;