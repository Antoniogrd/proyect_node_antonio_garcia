const express = require('express');
const router = express.Router();
controller = require('../controllers/brands.controllers')
const { isAdmin } = require('../middlewares/auth.middleware');
const {upload} = require('../middlewares/file.middleware');

router.get('/', controller.brandsGet);

// Create
router.get('/create-brand', isAdmin, controller.createBrandGet)
router.post('/create-brand', isAdmin, controller.createBrandPost);

// Read
router.get('/:id', controller.idBrandGet);
router.get('/:name', controller.nameBrandGet);

// Update
router.put('/edit', isAdmin, controller.updateBrandPut);

// Delete
router.post('/:id', isAdmin, controller.deleteBrandPost);

// Añadir modelo
router.put('/add-model', isAdmin, async (req, res, next) => {
    
    try{
        const { brandId, modelId } = req.body;

        const updatedBrand = await Brand.findByIdAndUpdate( //para añadir modelo, no sería así, findbyidandupdate es para modificar
        brandId,

        { $addToSet: { models: modelId } },

        { new: true }

        );

        return res.status(200).json(updatedBrand);

    } catch(error) {
        next(error);
    }
});

module.exports = router;
