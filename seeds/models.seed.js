const mongoose = require('mongoose');
const Model = require('../models/Model.model');
const { DB_URL, DB_CONFIG } = require('../db');
const fetch = require("node-fetch");


const getCars = async () => {
    let modelsList2 = [];
    await fetch("https://private-anon-11d25ae988-carsapi1.apiary-mock.com/cars")
    .then(res => res.json())
    .then((res) => {
        // console.log('console log de res', res)

        const brandList2 = ["subaru", "rolls-royce", "audi", "porshe", "maserati"];

        const carList = res;
        carList.forEach((car) => {

            if(brandList2.includes(car.make)){
                modelsList2.push({
                make: car.make,
                model:  car.model,
                year: car.year,
                horsepower: car.horsepower,
                price: car.price,
                image: car.img_url,   
                });
            }  
        })
    })
    .catch(error => {
        console.log('no se pudo completar la petición fetch', error)
    })
    // year, model, horsepower, price, image
    return modelsList2
}

const grabarBD = async() => {
const modelsList2 = await getCars()

mongoose.connect(DB_URL, DB_CONFIG)

    .then(async () => {

        console.log('Ejecutando seed Models.js');

        // Comprobamos si existen Models
        const allModels = await Model.find();

        // Si existen, las eliminamos
        if (allModels.length) {
            await Model.collection.drop();
            console.log('Colección de Models eliminada con éxito');
        }
    })

    .catch(error => {

        // Si hubiese algún error, lo notificamos
        console.log('error buscando en la DB:', error);
    })

    .then(async () => {

        // Insertamos los Models del array
        await Model.insertMany(modelsList2);
        console.log('Añadidas nuevos Models a DB');
    })

    .catch((error) => {

        // Notificamos en caso de error
        console.log('Error insertando Models', error)
    })

    .finally(() => {

        // Cambio y corto
        mongoose.disconnect()
    });
}

grabarBD()