const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// const modelSchema = new Schema(
//     {
//         year: { type: Number },
//         model: { type: String, required: true },
//         horsepower: { type: Number, required: true },
//         price: { type: Number, required: true },
//         image: { type: String, required: true}
//     },
//     {
//         timestamp: true,
//     }
// );

const modelSchema = new Schema(
    {
        make: { type: String },
        // make: [{ type: mongoose.Types.ObjectId, ref: 'Brands' }],
        model: { type: String },
        year: { type: Number },
        horsepower: { type: Number },
        price: { type: Number },
        image: { type: String }
    },
    {
        timestamp: true,
    }
);

const Model = mongoose.model('Models', modelSchema);

module.exports = Model;

// make
// model 
// year
// horsepower
// price
// image