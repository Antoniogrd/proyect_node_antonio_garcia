const mongoose = require ('mongoose');

const Schema = mongoose.Schema;

const brandSchema = new Schema(
    {
        name: { type: String, required: true},
        // name: [{ type: mongoose.Types.ObjectId, ref: 'Models' }],
        location: { type: String, required: true},
        avg_horsepower: { type: Number, required: true},
        avg_price: { type: Number, required: true },
        logo: {type: String }
    },
    {
        timestamp: true,
    }
);

const Brand = mongoose.model('Brands', brandSchema);

module.exports = Brand;


// name
// location 
// avg_horsepower 
// avg_price 
// logo


// subaru: 7, rolls-royce: 6, audi: 7, porshe: 8, maserati: 4