const express = require('express');
const path = require('path');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const passport = require('passport');
const dotenv = require('dotenv');
dotenv.config();
// const hbs = require('hbs')

const PORT = process.env.PORT || 3000;

const indexRoutes = require('../proyect-cars/routes/index.routes');
const authRoutes = require('../proyect-cars/routes/auth.routes');
const modelRoutes = require('../proyect-cars/routes/models.routes');
const brandRoutes = require('../proyect-cars/routes/brands.routes');

const db = require('./db.js');
// const { Console } = require('console');
require('../proyect-cars/authentication');

db.connect();

const server = express();
// Guarda una sesión del usuario en nuestro servidor. Quitarla, no debe estar aquí
server.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 5 * 24 * 60 * 60 * 1000,
        },
        store: MongoStore.create({ mongoUrl: db.DB_URL}),
    })
);


server.use(passport.initialize()); 
server.use(passport.session());


server.use(express.json());
server.use(express.urlencoded({ extend: true}));

server.set('views', path.join(__dirname, 'views')); // me concatena la dirección del servidor con views, para indicarle el directorio a path.join
server.set('view engine', 'hbs'); // le decimos el motor de vista que vamos a usar, en éste caso será hbs

// hbs.registerHelper('mayorque', (a, b, options) => {
//     if (a > b) {
//         return options.fn(this);
//     } else {
//         options.inverse(this);
//     }
// });
server.use(express.static(path.join(__dirname, 'public')))

server.use('/', indexRoutes);
server.use('/auth', authRoutes);
server.use('/models', modelRoutes);
server.use('/brands', brandRoutes);

// router.get('/brands', (req, res) => {
//     return res.send('Bienvenido, éstos son nuestras marcas!');
// })

// router.get('/brands/:name', (req, res) => {
//     const name = req.params.name;

//     const audiModels = ['A3', "A4", "A6", "Q5", "Q7", "R8", "TT"];

//     const hasCar = audiModels.some((car) => car.toLowerCase() === name.toLowerCase());

//     if(hasCar) {
//         return res.send(`We have this model car! ${name}`);
//     } else {
//         return res.send('We could not find the model in the database!')
//     }
// });



server.use('*', (req, res) => {
    const error = new Error('Route not found');
    error.status = 404;
    return res.status(404).json(error.message);
});

server.use((error, req, res, next) => {
    console.log('Error Handler', error);
    const errorStatus = error.status || 500;
    const errorMsg = error.message || 'Unexpected Error';
    return res.render('error', { status: errorStatus, message: errorMsg })
});

server.listen(PORT, () => {
    console.log(`Server runnig in http://localhost:${PORT}`);
});