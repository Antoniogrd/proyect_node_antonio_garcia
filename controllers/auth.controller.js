const passport = require('passport');

module.exports = {
    registerGet: (req, res, next) => {
        return res.render('register');
    },

    registerPost: (req, res, next) => {

        // Controlamos el registro, si hay error mandamos error y si no, enviamos el usuario
        const done = (error, user) => {
    
            if (error) {
                return next(error);
            };
    
            req.logIn(user, (error) => {
                if (error) {
                    return next(error);
                }
                // return res.status(201).json(user);
                return res.redirect('/');
            });
        };
    
        passport.authenticate('registro', done)(req);
    },

    loginGet: (req, res, next) => {
        return res.render('login');
    },

    loginPost: (req, res, next) => {
        passport.authenticate('acceso', (error, user) => {
            if(error) {
                return next(error);
            }
    
            req.logIn(user, (error) => {
                if(error) {
                    return next(error);
                }
    
                return res.redirect('/')
            });
        })(req);
    },

    logoutPost: (req, res, next) => {

        // Desconexión, limpiamos las cookies y deslogueamos al usuario
        if (req.user){
            req.logout();
    
            req.session.destroy(() => {
                res.clearCookie('connect.sid');
    
                return res.redirect('/');
                // return res.status(200).json('User successfully logged out');
            });
        } else {
            return res.status(200).json('There was no user logged in');
        }
    }
}