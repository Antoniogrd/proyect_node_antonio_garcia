const indexGet = (req, res) => {
    const carBrands = [`Audi, Bmw, Mercedes, Chrysler, Porsche`];
    // return res.send(`Bienvenido a nuestra tienda!, disponemos de las siguientes marcas: ${carBrands}`);
    return res.render('index', { brands: carBrands, user: req.user });
}

module.exports = { indexGet };