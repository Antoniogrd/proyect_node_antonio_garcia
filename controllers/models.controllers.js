const Model = require('../models/Model.model');

module.exports = {

    modelsGet: async (req, res, next) => {
    
        try {
            const models = await Model.find();
            // return res.status(200).json(models);
            return res.render('models', { models : models })
    
        } catch (error) {
            next(error);
        }
    },

    createModelGet : (req, res, next) => {
        return res.render('create-model');
    },

    createModelPost : async (req, res, next) => {
    
        try {
            // Destructuring con los datos recibidos del post
            const { year, model, horsepower, price, image } = req.body;
    
            // Creamos instancia de Model con los campos necesarios
            const newModel = new Model({ year, model, horsepower, price, image });
    
            // Guardamos en la BD el nuevo Modelo
            const createdModel = await newModel.save();
    
            return res.status(201).json(createdModel);
    
        } catch (error) {
            return next(error);
        }
    },
    
    idModelGet : async (req, res, next) => { 
    
        const { id } = req.params;
        
        try {
            const model = await Model.findById(id);
            // return res.json(model);
            return res.render(model);
            
        } catch(error) {
            return next(error);
        }
    },

    updateModelPut : async (req, res, next) => {
    
        try {
            const { id, name } = req.body;
    
            const editedModel = await Model.findByIdAndUpdate(
                id, 
                { name },
                { new: true}
            );
            return res.status(200).json(editedModel);
    
         } catch (error) {
             return next(error);
         }
    },

    deleteModelPost : async (req, res, next) => {
    
        try {
            const { id } = req.params;
            await Model.findByIdAndDelete(id);
    
            return res.redirect('/models');
            // return res.status(200).json('Model deleted');
    
        } catch(error) {
            next(error);
        }
    }
    
}