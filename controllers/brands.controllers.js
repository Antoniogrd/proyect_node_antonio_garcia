const Brand = require('../models/Brand.model');

module.exports = {

    brandsGet: async (req, res, next) => {
    
        try {
            const brands = await Brand.find().populate('Models');
            // return res.status(200).json(brands);
            return res.render('brands', { brands: brands }) // con res.render, le decimos que nos renderice el archivo "brands"
            // la propiedaad brands, tiene como valor la variable de arriba 
    
        } catch (error) {
           return next(error);
        }
    },

    createBrandGet: (req, res, next) => {
        return res.render('create-brand');
    },

    createBrandPost: async (req, res, next) => {
        // router.post('/create', [isAdmin, upload.single('image')], async (req, res, next) => { //versión multer
        
        try {
            // Destructuring con los datos recibidos del post
            const { name, location, avg_horsepower, avg_price, logo } = req.body;
    
            // logo = req.file ? req.file.filename : 'https://www.mothers.mx/Content/images/productos/big01/nodisponible.jpg' // multer
    
            // Creamos instancia de Brand con los campos necesarios
            const newBrand = new Brand({ name, location, avg_horsepower, avg_price, logo }); //aquí debería de añadir la variable anterior, pero no sé si petará
    
            // Guardamos en la BD la nueva Brand
            const createdBrand = await newBrand.save();
    
            return res.render('brand', { brand: createdBrand});
    
            // return res.status(201).json(createdBrand);
    
        } catch (error) {
            return next(error);
        }
    },

    idBrandGet: async (req, res, next) => {
    
        const { id } = req.params;
        
        try {
            const brand = await Brand.findById(id);
            // return res.json(brand);
            return res.render('models', { brand } );
            
        } catch(error) {
            return next(error);
        }
    },

    nameBrandGet: (req, res) => {
        const name = req.params.name;
    
        const brands = ['Subaru', "Rolls-Royce", "Audi", "Porshe", "Maserati"];
    
        const hasBrand = brands.some((brand) => brand.toLowerCase() === name.toLowerCase());
    
        if(hasBrand) {
            return res.send(`We have this model brand! ${name}`);
        } else {
            return res.send('We could not find the brand in the database!')
        }
    },

    updateBrandPut: async (req, res, next) => {
    
        try {
            const { id, name } = req.body;
    
            const editedBrand = await Brand.findByIdAndUpdate(
                id, 
                { name },
                { new: true}
            );
            return res.status(200).json(editedBrand);
    
         } catch (error) {
             return next(error);
         }
    },

    deleteBrandPost: async (req, res, next) => { // cambiado el delete por post para que funcione
    
        try {
            const { id } = req.params;
            await Brand.findByIdAndDelete(id);
    
            return res.redirect('/brands');
            // return res.status(200).json('Brand deleted');
    
        } catch(error) {
            next(error);
        }
    }

}